OBJS = hello_world.o
CC= g++
DEBUG= 
#-g
CFLAGS = -c $(DEBUG)
LFLAGS= $(DEBUG)
stdfac= std_lib_facilities.h

simple1: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o simple1
	
first.o: first.cpp $(stdfac)
	$(CC) $(CFLAGS) first.cpp
	
hello_world.o: hello_world.cpp $(stdfac)
	$(CC) $(CFLAGS) hello_world.cpp -o hello_world.o

simple2.o: second.cpp $(stdfac)
	$(CC) second.cpp -o simple2.o
drill1.o: drill1.cpp $(stdfac)
	$(CC) drill1.cpp -o drill1.o
unsafe.o: unsafe1.cpp $(stdfac)
	$(CC) unsafe1.cpp -o unsafe.o
